"""DaiBit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$','base.views.index',name='index'),
    url(r'^register/$','base.views.user_register',name='user_register'),
    url(r'^login/$','base.views.user_login',name='user_login'),
    url(r'^logout/$','base.views.user_logout',name='user_logout'),
    url(r'^upload/$','base.views.upload_audio',name='upload_audio'),
    url(r'^user/(?P<id>\d\d|\d)/$','base.views.profile_get',name='profile_get'),
    url(r'^uploads/(?P<user_id>\d\d|\d)/(?P<song_id>\d\d|\d)/$','base.views.record_get',name='record_get'),
    #url(r'^profile/$','base.views.profile_get',name='profile_get'),


]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

