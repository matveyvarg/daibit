from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


def u_path(instanse, filename):
	return '/upload/{0}/{1}'.format(instanse.author.id, filename)

class UserProfile(models.Model):

	user = models.OneToOneField(User,related_name='user_profile')

	website = models.URLField(blank=True)
	profile_image = models.ImageField(upload_to='profile_images', blank=True)
	
	is_confirmed = models.BooleanField(default=False)
	rating = models.FloatField(default=0)
	records = models.IntegerField(default=0)
	def __unicode__(self):
		return self.user.username

class Record(models.Model):

	title = models.CharField(max_length=128)
	author = models.ForeignKey(UserProfile,related_name='record')
	description = models.CharField(max_length=128)
	duration = models.IntegerField(default=0)
	likes = models.IntegerField(default=0)
	price = models.DecimalField(max_digits=10,decimal_places=2,default=0)
	file = models.FileField()
	def __unicode__(self):
		return self.title