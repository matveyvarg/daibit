# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-04-17 10:53
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_auto_20160414_1922'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='UserProile',
            new_name='UserProfile',
        ),
    ]
