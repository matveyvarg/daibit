from django import forms 

from .models import User, Record

#def u_path(instanse, filename):
#	return '{0}/{1}'.format(instanse.user.id, filename)

class UserAuth(forms.ModelForm):
	class Meta:
		model = User
		fields = ['username','email','password']

class RecordUpload(forms.ModelForm):
	class Meta:
		model = Record
		fields = ['title','file','author']

