from django.shortcuts import render, render_to_response, get_list_or_404, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from django.template import RequestContext

from base.forms import UserAuth, RecordUpload
from base.helpers import *
import json
from wsgiref.util import FileWrapper

# Create your views here.
def index(request):
	context = RequestContext(request)
	users = UserProfile.objects.order_by('rating').reverse()[:5]
	records = Record.objects.order_by('rating').reverse()[:5]
	return render_to_response('index.html',{'users':users,'records':records},context)

def user_register(request):

	data = {"nextpage":"/","form_errors":[],"auth_errors":[]}
	context = RequestContext(request)

	if request.method == 'POST':
		


		userform = UserAuth(data = request.POST)
		username = request.POST['username']
		password = request.POST['password']

		if userform.is_valid():

			user = userform.save()
			user.set_password(user.password)
			user.save()

			user = User.objects.get(username=username)
			print user.username
			user_profile = UserProfile(user=user)
			user_profile.save()

			user_auth = authenticate(username=username,password=password)

			if user_auth:
				
				if user_auth.is_active:

					login(request,user_auth)
					data['status'] = 'ok'
				else:
					data['status'] = 'errors'
					data['auth_errors'].append('user is not active')

			else:
				data['status'] = 'errors'
				data ['auth_errors'].append('Auth Failed')
				
		else:
			data['status'] = 'errors'
			data['form_errors'].append(userform.errors)
			


		return HttpResponse(json.dumps(data,ensure_ascii=False))

	else:

		userform = UserAuth()

	return render_to_response('register.html',context)  

def user_login(request):

	context = RequestContext(request)

	if request.method == 'POST':

		data = {"nextpage":"/"}
		email = request.POST['email']
		password = request.POST['password']

		_user = User.objects.get(email=email)

		user = authenticate(username = _user.username, password=password)
		if user:
			if user.is_active:
				login(request,user)
				data['status'] = 'ok'
			else:
				data['status'] = 'errors'
				data['errors'].append('User is an active')
		else:
			data['errors'].append('Auth Failed')

		return HttpResponse(json.dumps(data))
	else:
		return render_to_response('login.html',context)




@login_required
def user_logout(request):

	logout(request)

	return HttpResponseRedirect('/')


def profile_get(request,id):
	data = {}
	context = RequestContext(request)
	
	if request.user.is_authenticated() and int(id)==int(request.user.id):

		user = User.objects.get(username = request.user.username)
		data['user'] = user
		
		rc = Record.objects.filter(author=user.user_profile)
		data['rc']=rc
		return render_to_response('my_profile.html',data,context)

	else:
		
		user = User.objects.get(id = id)
		records = user.user_profile.record.all()
		

		data['user'] = user
		data['records'] = records
		return render_to_response('profile.html',data,context)
	
	

@login_required
def upload_audio(request):

	context = RequestContext(request)

	if request.method == 'POST':

		#print request.FILES['file'].name
		record = File(request)
		if record.save_record():

			return HttpResponseRedirect('/')
		
		 
	
	return render_to_response('upload.html',context)

def record_get(requset,user_id,song_id):

	record = get_object_or_404(Record,id=song_id)
	print record.file
	wrapper = FileWrapper(open(str(record.file),'rb'))
	response = HttpResponse(wrapper, content_type='audio/mp3')
	response['Content-Length'] = os.path.getsize(str(record.file))
	
	return response

def one_rec(request,song_id):

	context = RequestContext(request)



	record = get_object_or_404(Record,id=song_id)
	author = get_object_or_404(UserProfile,user=request.user)

	print record.is_voted

	if record.likes !=0:
		curr_rate = record.rating/record.likes
	else:
		curr_rate = 0

	if request.method == 'POST':

		author.is_voted = record

		record.likes +=1
		record.rating = (record.rating + int(request.POST['vote']))

		records=get_list_or_404(Record,author=author)
		print len(records)
		for rec in records:
			author.rating = author.rating + rec.rating
		author.rating  = author.rating/len(records)

		author.save()
		record.save()

		return HttpResponse(json.dumps({'status':'ok'}))
	else:
		return render_to_response('one_song.html',{'rec':record,'rating':curr_rate},context)



