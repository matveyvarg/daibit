from base.models import *

import os
class File:

	def __init__ (self,requset):
		self.__path  = 'uploads/' + str(requset.user.id)
		self.__file = requset.FILES['file']
		self.__name = requset.POST['title']
		self.__author = requset.user.username
		self.__destination = self.__path + "/" + self.__name


	def save_record(self):
		if not os.path.exists(self.__path):
			os.makedirs(self.__path)

		pth = str(self.__destination) + ".mp3"
		
		f = open(pth,"wb+")
		for chunk in self.__file.chunks():
			f.write(chunk) 
		
		user = User.objects.get(username=self.__author)
		user_profile = UserProfile.objects.get(user=user)
		rec = Record()
		rec.title = self.__name
		rec.author = user_profile
		
		rec.file = pth
		rec.save()

		user_profile.records +=1
		user_profile.save()

		fe = Record.objects.get(title=self.__name)
		
		return os.path.exists(pth)

class Debug:

	def __init__(self,requset):
		self.__user = requset.user.username

	def show_user(self):
		user = User.objects.get(username=self.__user)
		
		user_profile = UserProfile.objects.get(user=user)
		print user_profile.user
		print user_profile.user.email
		print user_profile.records
		if user_profile.records !=0:
			record = Record.objects.filter(author=user_profile)
			for rec in record:
				print rec.title
				print "Author:" + str(rec.author)