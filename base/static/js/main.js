'use strict'
var langSats = {};

langSats.menuBtn = function($){

	var $menu = $('i.fa-align-justify'),
		$select = $('#lang-list'),
		inYear = [];
		//Add optins to select
		$.ajax({

			dataType: "json",
			url:'http://localhost:8000/get_langs',
			success:function(data){

				$.each(data,function(){
					$select.append('<li><a href="#"">'+this.name+'</a></li>')
					inYear.push(this.vacs);
					console.log(data);
				});
				
				
			}
		});	


}




langSats.tooglMenu = function($){
	var isToggled = false;

	$('i#menu-toogle').click(function() {

		$('.nav-tabs').slideToggle("fast");
		console.log(isToggled);
		if(!isToggled) {
			
			isToggled = true;
            $('i#menu-toogle').removeClass("fa fa-angle-double-down fa-3x");
            $('i#menu-toogle').addClass("fa fa-angle-double-up fa-3x");
            
        }
        else{
	        if (isToggled) {
	        	
	        	$('i#menu-toogle').removeClass("fa fa-angle-double-up fa-3x");
	            $('i#menu-toogle').addClass("fa fa-angle-double-down fa-3x");
	            isToggled = false;
	            
	        };
    	}
    });
}

langSats.addCursor = function($){
	$("nav ul").on("mouseenter","li",function(){
		$(this).append('<div class="cursor"><i class="fa fa-sort-asc"></i></div>');
	});
	$("nav ul").on("mouseleave","li",function(){
		$('.cursor').empty();
	});

}

$(document).ready(function(){
	langSats.addCursor($);
	langSats.menuBtn($);
	langSats.tooglMenu($);
	langSats.Chart($);
	
})(jQuery);